### Description

Describe which feature you'd like to see.
Show some code examples how to show how the plugin should be behave.

### Breaking changes

Describe the known impacts here. Does this break the current behavior and if
yes why is it necessary?
