{
  description = "Customized i3lock-pixeled";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-21.11";

    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix?rev=6799201bec19b753a4ac305a53d34371e497941e";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = { self, nixpkgs, flake-utils, pre-commit-hooks }:
    let
      name = "i3lock-pixeled";
      overlay = final: prev: {
        "${name}" = prev."${name}".overrideAttrs (old: {
          src = builtins.path {
            inherit name;
            path = ./.;
          };
        });
      };
    in
    flake-utils.lib.eachSystem
      [ "aarch64-linux" "i686-linux" "x86_64-linux" ]
      (system:
        let
          pkgs = nixpkgs.legacyPackages."${system}";

          package = with pkgs; pkgs.stdenvNoCC.mkDerivation rec {

            inherit name;
            src = self;

            propagatedBuildInputs = [
              i3lock
              imagemagick
              scrot
              playerctl
            ];

            makeFlags = [
              "PREFIX=$(out)/bin"
            ];

            patchPhase = ''
              substituteInPlace i3lock-pixeled \
                 --replace i3lock    "${i3lock}/bin/i3lock" \
                 --replace convert   "${imagemagick}/bin/convert" \
                 --replace scrot     "${scrot}/bin/scrot" \
                 --replace playerctl "${playerctl}/bin/playerctl"
            '';
          };
        in
        rec {
          apps."${name}" = flake-utils.lib.mkApp { drv = packages."${name}"; };

          defaultApp = apps."${name}";

          packages."${name}" = package;

          defaultPackage = packages."${name}";

          checks = {
            build = (
              import nixpkgs {
                inherit system;
                overlays = [ overlay ];
              }
            )."${name}";

            pre-commit-check = pre-commit-hooks.lib."${system}".run {
              src = ./.;
              hooks = {
                nixpkgs-fmt.enable = true;
                statix.enable = true;
              };
            };

            shellcheck = with pkgs; runCommand "shellcheck" { } ''
              ${pkgs.shellcheck}/bin/shellcheck --enable all ${./.}/i3lock-pixeled
              touch ${placeholder "out"}
            '';
          };

          devShell = pkgs.mkShell {
            inherit name;

            buildInputs = with pkgs; [
              # banner printing on enter
              figlet
              lolcat

              nixpkgs-fmt
              shellcheck
              statix

              packages."${name}"
            ];

            shellHook = ''
              figlet ${name} | lolcat --freq 0.5
              ${checks.pre-commit-check.shellHook}
            '';
          };
        }) // {
      inherit overlay;
    };
}
